<?php
// HTTP
define('HTTP_SERVER', 'http://localhost:8888/admin/');
define('HTTP_CATALOG', 'http://localhost:8888/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost:8888/admin/');
define('HTTPS_CATALOG', 'http://localhost:8888/');

// DIR
define('DIR_APPLICATION', '/Users/mike/Documents/Sites/bitbucket/kehau/opencart/opencart-1.5.6.4/upload/admin/');
define('DIR_SYSTEM', '/Users/mike/Documents/Sites/bitbucket/kehau/opencart/opencart-1.5.6.4/upload/system/');
define('DIR_DATABASE', '/Users/mike/Documents/Sites/bitbucket/kehau/opencart/opencart-1.5.6.4/upload/system/database/');
define('DIR_LANGUAGE', '/Users/mike/Documents/Sites/bitbucket/kehau/opencart/opencart-1.5.6.4/upload/admin/language/');
define('DIR_TEMPLATE', '/Users/mike/Documents/Sites/bitbucket/kehau/opencart/opencart-1.5.6.4/upload/admin/view/template/');
define('DIR_CONFIG', '/Users/mike/Documents/Sites/bitbucket/kehau/opencart/opencart-1.5.6.4/upload/system/config/');
define('DIR_IMAGE', '/Users/mike/Documents/Sites/bitbucket/kehau/opencart/opencart-1.5.6.4/upload/image/');
define('DIR_CACHE', '/Users/mike/Documents/Sites/bitbucket/kehau/opencart/opencart-1.5.6.4/upload/system/cache/');
define('DIR_DOWNLOAD', '/Users/mike/Documents/Sites/bitbucket/kehau/opencart/opencart-1.5.6.4/upload/download/');
define('DIR_LOGS', '/Users/mike/Documents/Sites/bitbucket/kehau/opencart/opencart-1.5.6.4/upload/system/logs/');
define('DIR_CATALOG', '/Users/mike/Documents/Sites/bitbucket/kehau/opencart/opencart-1.5.6.4/upload/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'grindz');
define('DB_PREFIX', 'oc_');
?>